terraform {
  backend "s3" {
    bucket = "BUCKETNAME"  //create a bucket in your aws acc
    key    = "NAME"
    region = "YOUR REGION"
  }
}

provider "aws" {
  region = local.region
}

locals {
  region = "eu-central-1"
  availability_zone = "${local.region}a"
  name = "YOUR NAME"

  tags = {                                      //tags for better sorting
    Owner       = "your_name"                   //your name
    Environment = "prod"                        //optional
    Account     = "NAME"                        //account in aws to deploy
    Team        = "data"                        //team
  }
}

################################################################################
# VPC Module
################################################################################

module "vpc" {  
  source  = "terraform-aws-modules/vpc/aws"
  version = "= 3.14.4"
  name = "Data_Pool_VPC"
  cidr = "10.1.0.0/16"

  azs             = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"]
  public_subnets  = ["10.1.0.0/24", "10.1.4.0/24"]

  enable_ipv6 = false

  enable_nat_gateway = false
  single_nat_gateway = true

  public_subnet_tags = {
    Name = "NAME"
  }
  private_subnet_tags = {
    Name = "NAME"
  }

  tags = local.tags

  vpc_tags = {
    Name = "NAME"
  }
}



module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "= 4.13.0"

  name        = local.name
  description = "Security group for example usage with EC2 instance"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["all-all"]
  egress_rules        = ["all-all"]

  tags = local.tags
}

################################################################################
# EC2 Module
################################################################################

module "ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"
  version = "= 4.1.4"

  name = local.name

  ami                         = "ami-02c6b57cb03112ebf"
  instance_type               = "t2.small"
  availability_zone           = local.availability_zone
  key_name                    = "NAME"
  subnet_id                   = element(module.vpc.public_subnets, 0)
  vpc_security_group_ids      = [module.security_group.security_group_id]
  associate_public_ip_address = true

  tags = local.tags
}

resource "aws_volume_attachment" "this" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.this.id
  instance_id = module.ec2.id
}

resource "aws_ebs_volume" "this" {
  availability_zone = local.availability_zone
  size              = 1
  
  tags = local.tags
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = module.ec2.id
  allocation_id = "YOUR EXISTING EIP"
}
